energy_buildings_ai_allow = {
	has_any_tile_strategic_resource = no
	from = { NOT = { has_planet_flag = purged_planet } }
	OR = {
		AND = {
			new_building_content_enabled = no
			has_resource = { type = energy amount > 0 }
		}
		AND = {
			new_building_content_enabled = yes
			has_resource = { type = energy amount = 1 }
		}
		AND = {
			from = {
				NOT = {
					any_tile = {
						has_resource = { type = energy amount > 0 }
						has_building = no
						has_blocker = no
					}
				}
			}
			OR = { has_deposit = no allow_on_food_tile_conditions = yes }
		}
	}
}
minerals_buildings_ai_allow = {
	has_any_tile_strategic_resource = no
	from = { NOT = { has_planet_flag = purged_planet } }
	OR = {
		AND = {
			new_building_content_enabled = no
			has_resource = { type = minerals amount > 0 }
		}
		AND = {
			new_building_content_enabled = yes
			has_resource = { type = minerals amount = 1 }
		}
		AND = {
			from = {
				NOT = {
					any_tile = {
						has_resource = { type = minerals amount > 0 }
						has_building = no
						has_blocker = no
					}
				}
			}
			OR = { has_deposit = no allow_on_food_tile_conditions = yes }
		}
	}
}
society_research_buildings_ai_allow = {
	has_any_tile_strategic_resource = no
	from = { NOT = { has_planet_flag = purged_planet } }
	OR = {
		AND = {
			new_building_content_enabled = no
			has_resource = { type = society_research amount > 0 }
		}
		AND = {
			new_building_content_enabled = yes
			has_resource = { type = society_research amount = 1 }
		}
		AND = {
			from = {
				NOT = {
					any_tile = {
						has_resource = { type = society_research amount > 0 }
						has_building = no
						has_blocker = no
					}
				}
			}
			OR = { has_deposit = no allow_on_food_tile_conditions = yes }
		}
	}
}
physics_research_buildings_ai_allow = {
	has_any_tile_strategic_resource = no
	from = { NOT = { has_planet_flag = purged_planet } }
	OR = {
		AND = {
			new_building_content_enabled = no
			has_resource = { type = physics_research amount > 0 }
		}
		AND = {
			new_building_content_enabled = yes
			has_resource = { type = physics_research amount = 1 }
		}
		AND = {
			from = {
				NOT = {
					any_tile = {
						has_resource = { type = physics_research amount > 0 }
						has_building = no
						has_blocker = no
					}
				}
			}
			OR = { has_deposit = no allow_on_food_tile_conditions = yes }
		}
	}
}
engineering_research_buildings_ai_allow = {
	has_any_tile_strategic_resource = no
	from = { NOT = { has_planet_flag = purged_planet } }
	OR = {
		AND = {
			new_building_content_enabled = no
			has_resource = { type = engineering_research amount > 0 }
		}
		AND = {
			new_building_content_enabled = yes
			has_resource = { type = engineering_research amount = 1 }
		}
		AND = {
			from = {
				NOT = {
					any_tile = {
						has_resource = { type = engineering_research amount > 0 }
						has_building = no
						has_blocker = no
					}
				}
			}
			OR = { has_deposit = no allow_on_food_tile_conditions = yes }
		}
	}
}
unity_buildings_ai_allow = {
	has_any_tile_strategic_resource = no
	from = { NOT = { has_planet_flag = purged_planet } }
	OR = {
		has_resource = { type = unity amount > 0 }
		AND = {
			from = {
				NOT = {
					any_tile = {
						has_resource = { type = unity amount > 0 }
						has_building = no
						has_blocker = no
					}
				}
			}
			OR = { has_deposit = no allow_on_food_tile_conditions = yes }
		}
		AND = {
			from = {
				NOT = {
					any_tile = {
						OR = {
							has_resource = { type = unity amount > 0 }
							has_resource = { type = food amount = 1 }
							has_deposit = no
						}
						has_building = no
						has_blocker = no
					}
				}
			}
			has_resource = { type = food amount > 0 }
		}
		AND = {
			from = {
				NOT = {
					any_tile = {
						OR = {
							has_resource = { type = unity amount > 0 }
							has_resource = { type = food amount > 0 }
							has_deposit = no
						}
						has_building = no
						has_blocker = no
					}
				}
			}
			OR = {
				AND = {
					has_resource = { type = energy amount > 0 }
					has_resource = { type = energy amount < 3 }
				}
				AND = {
					has_resource = { type = minerals amount > 0 }
					has_resource = { type = minerals amount < 3 }
				}
			}
		}
	}
}
food_and_energy_buildings_ai_allow = {
	has_any_tile_strategic_resource = no
	from = { NOT = { has_planet_flag = purged_planet } }
	OR = {
		AND = {
			new_building_content_enabled = no
			OR = {
				has_resource = { type = food amount > 0 }
				has_resource = { type = energy amount > 0 }
			}
		}
		AND = {
			new_building_content_enabled = yes
			OR = {
				has_resource = { type = food amount = 1 }
				has_resource = { type = energy amount = 1 }
			}
		}
		AND = {
			has_deposit = no
			from = {
				NOT = {
					any_tile = {
						OR = {
							AND = {
								new_building_content_enabled = no
								OR = {
									has_resource = { type = food amount > 0 }
									has_resource = { type = energy amount > 0 }
								}
							}
							AND = {
								new_building_content_enabled = yes
								OR = {
									has_resource = { type = food amount = 1 }
									has_resource = { type = energy amount = 1 }
								}
							}
						}
						has_building = no
						has_blocker = no
					}
				}
			}
		}
		AND = {
			OR = {
				has_resource = { type = minerals amount < 2 }
				has_resource = { type = society_research amount < 2 }
				has_resource = { type = physics_research amount < 2 }
				has_resource = { type = engineering_research amount < 2 }
			}
			from = {
				NOT = {
					any_tile = {
						OR = {
							AND = {
								new_building_content_enabled = no
								OR = {
									has_resource = { type = food amount > 0 }
									has_resource = { type = energy amount > 0 }
									has_deposit = no
								}
							}
							AND = {
								new_building_content_enabled = yes
								OR = {
									has_resource = { type = food amount = 1 }
									has_resource = { type = energy amount = 1 }
									has_deposit = no
								}
							}
						}
						has_building = no
						has_blocker = no
					}
				}
			}
		}
	}
}
energy_and_minerals_buildings_ai_allow = {
	OR = { energy_buildings_ai_allow = yes minerals_buildings_ai_allow = yes }
}
energy_and_unity_buildings_ai_allow = {
	OR = { energy_buildings_ai_allow = yes unity_buildings_ai_allow = yes }
}
society_and_unity_buildings_ai_allow_conditions = {
	OR = { society_research_buildings_ai_allow = yes unity_buildings_ai_allow = yes }
}
engineering_and_unity_buildings_ai_allow_conditions = {
	OR = { engineering_research_buildings_ai_allow = yes unity_buildings_ai_allow = yes }
}
physics_and_unity_buildings_ai_allow_conditions = {
	OR = { physics_research_buildings_ai_allow = yes unity_buildings_ai_allow = yes }
}
food_and_unity_buildings_ai_allow = {
	OR = {
		AND = {
			new_building_content_enabled = no
			has_resource = { type = food amount > 0 }
		}
		AND = {
			new_building_content_enabled = no
			has_resource = { type = food amount = 1 }
		}
		unity_buildings_ai_allow = yes
	}
}
general_research_buildings_ai_allow = {
	OR = {
		society_research_buildings_ai_allow = yes
		engineering_research_buildings_ai_allow = yes
		physics_research_buildings_ai_allow = yes
	}
}
capital_buildings_ai_allow = {
	has_any_tile_strategic_resource = no
	from = { NOT = { has_planet_flag = purged_planet } }
	num_adjacent_tiles > 2
	any_neighboring_tile = { has_resource = yes }
	OR = {
		has_resource = { type = energy amount > 0 }
		has_resource = { type = unity amount > 0 }
		has_resource = { type = food amount > 0 }
		AND = {
			has_deposit = no
			from = {
				NOT = {
					any_tile = {
						OR = {
							has_resource = { type = energy amount > 0 }
							has_resource = { type = unity amount > 0 }
							has_resource = { type = food amount > 0 }
						}
						has_building = no
						has_blocker = no
					}
				}
			}
		}
	}
}
slave_processing_ai_allow = {
	has_any_tile_strategic_resource = no
	from = { NOT = { has_planet_flag = purged_planet } }
	OR = {
		AND = {
			new_building_content_enabled = no
			OR = {
				has_resource = { type = food amount > 0 }
				has_resource = { type = minerals amount > 0 }
			}
		}
		AND = {
			new_building_content_enabled = yes
			OR = {
				has_resource = { type = food amount = 1 }
				has_resource = { type = minerals amount = 1 }
			}
		}
		AND = {
			has_deposit = no
			from = {
				NOT = {
					any_tile = {
						OR = {
							AND = {
								new_building_content_enabled = no
								OR = {
									has_resource = { type = food amount > 0 }
									has_resource = { type = minerals amount > 0 }
								}
							}
							AND = {
								new_building_content_enabled = yes
								OR = {
									has_resource = { type = food amount = 1 }
									has_resource = { type = minerals amount = 1 }
								}
							}
						}
						has_building = no
						has_blocker = no
					}
				}
			}
		}
		AND = {
			OR = {
				has_resource = { type = energy amount < 2 }
				has_resource = { type = society_research amount < 2 }
				has_resource = { type = physics_research amount < 2 }
				has_resource = { type = engineering_research amount < 2 }
			}
			from = {
				count_owned_pop = {
					limit = {
						is_enslaved = no
						is_being_purged = no
						is_sapient = yes
					}
					count < 4
				}
				NOT = {
					any_tile = {
						OR = {
							AND = {
								new_building_content_enabled = no
								OR = {
									has_resource = { type = food amount > 0 }
									has_resource = { type = minerals amount > 0 }
									has_deposit = no
								}
							}
							AND = {
								new_building_content_enabled = yes
								OR = {
									has_resource = { type = food amount = 1 }
									has_resource = { type = minerals amount = 1 }
									has_deposit = no
								}
							}
						}
						has_building = no
						has_blocker = no
					}
				}
			}
		}
	}
}
military_academy_ai_allow_conditions = {
	OR = {
		AND = {
			overhauled_building_stats_enabled = no
			OR = { society_research_buildings_ai_allow = yes engineering_research_buildings_ai_allow = yes }
		}
		AND = { overhauled_building_stats_enabled = yes unity_buildings_ai_allow = yes }
	}
}
clone_vats_ai_allow_conditions = { society_research_buildings_ai_allow = yes }
war_factor_ai_allow_conditions = {
	OR = {
		AND = {
			overhauled_building_stats_enabled = no
			OR = { society_research_buildings_ai_allow = yes engineering_research_buildings_ai_allow = yes }
		}
		AND = { overhauled_building_stats_enabled = yes unity_buildings_ai_allow = yes }
	}
}
planetary_shield_ai_allow_conditions = {
	OR = {
		AND = {
			overhauled_building_stats_enabled = no
			OR = { physics_research_buildings_ai_allow = yes engineering_research_buildings_ai_allow = yes }
		}
		AND = { overhauled_building_stats_enabled = yes unity_buildings_ai_allow = yes }
	}
}
replicator_ai_allow = {
	OR = {
		has_deposit = no
		AND = {
			from = {
				NOT = {
					any_tile = {
						has_deposit = no
						has_building = no
						has_blocker = no
					}
				}
			}
			allow_on_food_tile_conditions = yes
		}
	}
	from = {
		NOT = { has_planet_flag = purged_planet }
		count_tile = {
			limit = { has_building = building_replicator }
			count < 3
		}
	}
}
 ### New Building Content
hyperstorage_ai_allow = {
	has_no_deposit_or_allow_on_food = yes
	num_adjacent_tiles > 2
	any_neighboring_tile = {
		OR = {
			has_resource = { type = food amount > 0 }
			has_resource = { type = energy amount > 0 }
			has_resource = { type = minerals amount > 0 }
		}
	}
}
has_no_deposit_or_allow_on_food = {
	OR = { has_deposit = no allow_on_food_tile_conditions = yes }
}
allow_on_food_tile_conditions = {
	owner = {
		OR = {
			AND = { is_machine_empire = yes is_servitor = no }
			AND = {
				any_owned_planet = { has_growing_pop = yes }
				has_monthly_income = { resource = food value > 149 }
			}
			AND = {
				any_owned_planet = { has_growing_pop = no }
				has_monthly_income = { resource = food value > 50 }
			}
		}
	}
	OR = {
		AND = {
			new_building_content_enabled = no
			has_resource = { type = food amount > 0 }
			NOR = {
				has_resource = { type = minerals amount > 0 }
				has_resource = { type = energy amount > 0 }
				has_resource = { type = society_research amount > 0 }
				has_resource = { type = physics_research amount > 0 }
				has_resource = { type = engineering_research amount > 0 }
				has_resource = { type = unity amount > 0 }
			}
		}
		AND = {
			new_building_content_enabled = no
			has_resource = { type = food amount = 1 }
			NOR = {
				has_resource = { type = minerals amount > 0 }
				has_resource = { type = energy amount > 0 }
				has_resource = { type = society_research amount > 0 }
				has_resource = { type = physics_research amount > 0 }
				has_resource = { type = engineering_research amount > 0 }
				has_resource = { type = unity amount > 0 }
			}
		}
	}
}
 ### Complex Triggers
food_buildings_ai_allow = {
	has_any_tile_strategic_resource = no
	owner = {
		has_monthly_income = { resource = food value < 150 }
	}
	from = { NOT = { has_planet_flag = purged_planet } }
	OR = {
		has_resource = { type = food amount > 0 }
		AND = {
			has_deposit = no
			from = {
				NOT = {
					any_tile = {
						has_resource = { type = food amount > 0 }
						has_building = no
						has_blocker = no
					}
				}
			}
		}
		AND = {
			owner = {
				OR = {
					AND = { is_machine_empire = no is_synth_empire = no }
					has_monthly_income = { resource = food value < 0 }
				}
			}
			from = {
				any_tile = {
					NOR = {
						has_resource = { type = food amount > 0 }
						has_deposit = no
					}
					has_building = no
					has_blocker = no
				}
			}
		}
	}
}
planet_unique_prerequisite = {
	OR = {
		owner = {
			OR = {
				has_monthly_income = { resource = minerals value < 50 }
				has_monthly_income = { resource = energy value < 50 }
				AND = {
					OR = { is_machine_empire = no is_servitor = yes }
					has_monthly_income = { resource = food value < 20 }
				}
			}
		}
		from = {
			OR = {
				AND = {
					owner = { is_machine_empire = no is_spiritualist = no }
					OR = {
						has_building = building_autochthon_monument
						has_building = building_heritage_site
						has_building = building_hypercomms_forum
						has_building = building_autocurating_vault
						has_building = building_hab_cultural_center
					}
				}
				AND = {
					owner = { is_spiritualist = yes }
					OR = {
						has_building = building_temple
						has_building = building_holotemple
						has_building = building_sacred_nexus
						has_building = building_citadel_of_faith
						has_building = building_hab_cultural_center
					}
				}
				AND = {
					owner = { is_machine_empire = yes }
					OR = {
						has_building = building_uplink_node
						has_building = building_network_junction
						has_building = building_system_conflux
						has_building = building_alpha_hub
					}
				}
			}
			OR = {
				free_pop_tiles < 2
				has_growing_pop = no
				owner = {
					is_machine_empire = no
					NOT = { has_technology = "tech_frontier_health" }
				}
				owner = {
					is_machine_empire = yes
					NOT = { has_technology = "tech_modular_components" }
				}
				AND = {
					owner = { is_machine_empire = yes }
					OR = { has_building = building_spare_parts_depot has_building = building_unit_assembly_plant }
				}
				AND = {
					owner = { is_machine_empire = no }
					OR = { has_building = building_clinic has_building = building_hospital }
				}
			}
			OR = {
				NOR = {
					has_capital_1 = yes
					has_capital_2 = yes
					has_capital_3 = yes
					has_hab_capital = yes
				}
				OR = {
					owner = { NOT = { has_active_tradition = tr_synchronicity_cyber_comms } }
					NOT = { any_owned_pop = { has_trait = trait_cybernetic } }
					has_building = building_neuro_electric_amplifier
				}
				OR = {
					owner = { has_synchronicity_machine_center = no }
					has_building = building_control_center
				}
				OR = {
					owner = { has_utopian_dream = no }
					has_building = building_paradise_dome
				}
				OR = {
					owner = { has_diplomacy_alien_tourism = no }
					has_building = building_visitor_center
				}
				OR = {
					owner = { has_synchronicity_hive_mind_synapse = no }
					has_building = building_hive_synapse
				}
				OR = {
					owner = { has_purity_symbol_purity = no }
					has_building = building_symbol_purity
				}
				OR = {
					owner = { has_purity_symbol_purity = no }
					has_building = building_symbol_purity
				}
			}
			OR = {
				owner = { NOT = { has_technology = tech_akx_worm_2 } }
				has_building = building_akx_worm_2
			}
		}
	}
}
pop_growth_building_conditions = {
	from = {
		owner = {
			OR = {
				has_technology = "tech_frontier_health"
				has_technology = "tech_frontier_hospital"
				has_technology = "tech_modular_components"
				has_technology = "tech_intelligent_factories"
			}
		}
		free_pop_tiles > 2
		OR = {
			any_owned_pop = { is_robot_pop = no has_population_control = { value = no country = prev.owner } }
			AND = {
				owner = {
					OR = { has_authority = auth_machine_intelligence has_country_flag = synthethic_age }
					balance > 250
				}
				any_owned_pop = { is_robot_pop = yes }
			}
		}
	}
}
