 ### Effects in this file are ran on pop scope.
calculate_average_pop_multipliers = {
	set_variable = { which = pop_num value = 0 }
	every_owned_pop = {
		limit = {
			is_growing = no
			OR = {
				is_being_purged = no
				has_purge_type = { type = purge_displacement }
				has_purge_type = { type = purge_labor_camps }
			}
			OR = { is_sapient = yes is_robot_pop = yes }
		}
		PREV = {
			change_variable = { which = pop_num value = 1 }
		}
		check_pop_traits_rights_modifiers_vanilla_and_API = yes
	}
	 ### get the average planet pop multiplier by dividing the multiplier variables with the number of pop variable.
	divide_variable = { which = energy_mult_planet_pop value = pop_num }
	divide_variable = { which = minerals_mult_planet_pop value = pop_num }
	divide_variable = { which = food_mult_planet_pop value = pop_num }
	divide_variable = { which = unity_mult_planet_pop value = pop_num }
	divide_variable = { which = society_research_mult_planet_pop value = pop_num }
	divide_variable = { which = physics_research_mult_planet_pop value = pop_num }
	divide_variable = { which = engineering_research_mult_planet_pop value = pop_num }
}
check_vanilla_pop_traits = {
	if = {
		limit = { is_robot_pop = no }
		if = {
			limit = { has_trait = trait_syncretic_proles }
			prev = {
				change_variable = { which = minerals_mult_planet_pop value = 0.10 }
				change_variable = { which = food_mult_planet_pop value = 0.10 }
				change_variable = { which = energy_mult_planet_pop value = -0.1 }
				change_variable = { which = physics_research_mult_planet_pop value = -0.25 }
				change_variable = { which = engineering_research_mult_planet_pop value = -0.25 }
				change_variable = { which = society_research_mult_planet_pop value = -0.25 }
			}
		}
		if = {
			limit = { has_trait = trait_presapient_earthbound }
			prev = {
				change_variable = { which = energy_mult_planet_pop value = 0.1 }
			}
		}
		if = {
			limit = { has_trait = trait_presapient_natural_intellectuals }
			prev = {
				change_variable = { which = unity_mult_planet_pop value = 0.05 }
				change_variable = { which = physics_research_mult_planet_pop value = 0.1 }
				change_variable = { which = engineering_research_mult_planet_pop value = 0.1 }
				change_variable = { which = society_research_mult_planet_pop value = 0.1 }
				change_variable = { which = minerals_mult_planet_pop value = -0.1 }
			}
		}
		if = {
			limit = { has_trait = trait_presapient_proles }
			prev = {
				change_variable = { which = minerals_mult_planet_pop value = 0.10 }
				change_variable = { which = food_mult_planet_pop value = 0.10 }
				change_variable = { which = physics_research_mult_planet_pop value = -0.15 }
				change_variable = { which = engineering_research_mult_planet_pop value = -0.15 }
				change_variable = { which = society_research_mult_planet_pop value = -0.15 }
			}
		}
		if = {
			limit = { has_trait = trait_agrarian }
			prev = {
				change_variable = { which = food_mult_planet_pop value = 0.15 }
			}
		}
		if = {
			limit = { has_trait = trait_thrifty }
			prev = {
				change_variable = { which = energy_mult_planet_pop value = 0.15 }
			}
		}
		if = {
			limit = { has_trait = trait_industrious }
			prev = {
				change_variable = { which = minerals_mult_planet_pop value = 0.15 }
			}
		}
		if = {
			limit = { has_trait = trait_intelligent }
			prev = {
				change_variable = { which = engineering_research_mult_planet_pop value = 0.1 }
				change_variable = { which = physics_research_mult_planet_pop value = 0.1 }
				change_variable = { which = society_research_mult_planet_pop value = 0.1 }
			}
		}
		if = {
			limit = { has_trait = trait_natural_engineers }
			prev = {
				change_variable = { which = engineering_research_mult_planet_pop value = 0.15 }
			}
		}
		if = {
			limit = { has_trait = trait_natural_physicists }
			prev = {
				change_variable = { which = physics_research_mult_planet_pop value = 0.15 }
			}
		}
		if = {
			limit = { has_trait = trait_natural_sociologists }
			prev = {
				change_variable = { which = society_research_mult_planet_pop value = 0.15 }
			}
		}
		if = {
			limit = { has_trait = trait_traditional }
			prev = {
				change_variable = { which = unity_mult_planet_pop value = 0.1 }
			}
		}
		if = {
			limit = { has_trait = trait_quarrelsome }
			prev = {
				change_variable = { which = unity_mult_planet_pop value = -0.1 }
			}
		}
		if = {
			limit = { has_trait = trait_very_strong }
			prev = {
				change_variable = { which = minerals_mult_planet_pop value = 0.10 }
			}
		}
		if = {
			limit = { has_trait = trait_strong }
			prev = {
				change_variable = { which = minerals_mult_planet_pop value = 0.05 }
			}
		}
		if = {
			limit = { has_trait = trait_weak }
			prev = {
				change_variable = { which = minerals_mult_planet_pop value = -0.05 }
			}
		}
		if = {
			limit = { has_trait = trait_latent_psionic }
			prev = {
				change_variable = { which = engineering_research_mult_planet_pop value = 0.05 }
				change_variable = { which = physics_research_mult_planet_pop value = 0.05 }
				change_variable = { which = society_research_mult_planet_pop value = 0.05 }
				change_variable = { which = energy_mult_planet_pop value = 0.05 }
			}
		}
		if = {
			limit = { has_trait = trait_psionic }
			prev = {
				change_variable = { which = engineering_research_mult_planet_pop value = 0.10 }
				change_variable = { which = physics_research_mult_planet_pop value = 0.10 }
				change_variable = { which = society_research_mult_planet_pop value = 0.10 }
				change_variable = { which = energy_mult_planet_pop value = 0.10 }
			}
		}
		if = {
			limit = { has_trait = trait_nerve_stapled }
			prev = {
				change_variable = { which = minerals_mult_planet_pop value = 0.1 }
				change_variable = { which = food_mult_planet_pop value = 0.1 }
				change_variable = { which = unity_mult_planet_pop value = -0.5 }
				change_variable = { which = energy_mult_planet_pop value = -0.5 }
				change_variable = { which = physics_research_mult_planet_pop value = -0.75 }
				change_variable = { which = engineering_research_mult_planet_pop value = -0.75 }
				change_variable = { which = society_research_mult_planet_pop value = -0.75 }
			}
		}
		if = {
			limit = { has_trait = trait_fertile }
			prev = {
				change_variable = { which = unity_mult_planet_pop value = 0.1 }
			}
		}
		if = {
			limit = { has_trait = trait_erudite }
			prev = {
				change_variable = { which = engineering_research_mult_planet_pop value = 0.2 }
				change_variable = { which = physics_research_mult_planet_pop value = 0.2 }
				change_variable = { which = society_research_mult_planet_pop value = 0.2 }
			}
		}
	}
	if = {
		limit = { is_robot_pop = yes }
		if = {
			limit = { has_trait = trait_robotic_1 }
			prev = {
				change_variable = { which = engineering_research_mult_planet_pop value = -0.8 }
				change_variable = { which = physics_research_mult_planet_pop value = -0.8 }
				change_variable = { which = society_research_mult_planet_pop value = -0.8 }
				change_variable = { which = unity_mult_planet_pop value = -0.4 }
				change_variable = { which = energy_mult_planet_pop value = -0.4 }
				change_variable = { which = minerals_mult_planet_pop value = 0.10 }
			}
		}
		if = {
			limit = { has_trait = trait_robotic_2 }
			prev = {
				change_variable = { which = engineering_research_mult_planet_pop value = -0.4 }
				change_variable = { which = physics_research_mult_planet_pop value = -0.4 }
				change_variable = { which = society_research_mult_planet_pop value = -0.4 }
				change_variable = { which = unity_mult_planet_pop value = -0.2 }
				change_variable = { which = energy_mult_planet_pop value = -0.2 }
				change_variable = { which = minerals_mult_planet_pop value = 0.15 }
			}
		}
		if = {
			limit = { has_trait = trait_robotic_3 }
			prev = {
				change_variable = { which = engineering_research_mult_planet_pop value = 0.20 }
				change_variable = { which = physics_research_mult_planet_pop value = 0.20 }
				change_variable = { which = society_research_mult_planet_pop value = 0.20 }
				change_variable = { which = unity_mult_planet_pop value = 0.20 }
				change_variable = { which = energy_mult_planet_pop value = 0.20 }
				change_variable = { which = minerals_mult_planet_pop value = 0.20 }
			}
		}
		if = {
			limit = { has_trait = trait_robot_power_drills }
			prev = {
				change_variable = { which = minerals_mult_planet_pop value = 0.10 }
			}
		}
		if = {
			limit = { has_trait = trait_robot_harvesters }
			prev = {
				change_variable = { which = food_mult_planet_pop value = 0.10 }
			}
		}
		if = {
			limit = { has_trait = trait_robot_logic_engines }
			prev = {
				change_variable = { which = society_research_mult_planet_pop value = 0.10 }
				change_variable = { which = physics_research_mult_planet_pop value = 0.10 }
				change_variable = { which = engineering_research_mult_planet_pop value = 0.10 }
			}
		}
		if = {
			limit = { has_trait = trait_robot_superconductive }
			prev = {
				change_variable = { which = energy_mult_planet_pop value = 0.10 }
			}
		}
		if = {
			limit = { has_trait = trait_robot_propaganda_machines }
			prev = {
				change_variable = { which = unity_mult_planet_pop value = 0.15 }
			}
		}
		if = {
			limit = { has_trait = trait_robot_awoken }
			prev = {
				change_variable = { which = society_research_mult_planet_pop value = 0.30 }
				change_variable = { which = physics_research_mult_planet_pop value = 0.30 }
				change_variable = { which = engineering_research_mult_planet_pop value = 0.30 }
			}
		}
		if = {
			limit = { has_trait = trait_robot_awoken_01 }
			prev = {
				change_variable = { which = society_research_mult_planet_pop value = 0.30 }
				change_variable = { which = physics_research_mult_planet_pop value = 0.30 }
				change_variable = { which = engineering_research_mult_planet_pop value = 0.30 }
			}
		}
	}
}
vanilla_pop_modifiers = {
	if = {
		limit = { has_modifier = pop_ghost_signal_1 }
		prev = {
			change_variable = { which = minerals_mult_planet_pop value = -0.10 }
			change_variable = { which = energy_mult_planet_pop value = -0.10 }
			change_variable = { which = physics_research_mult_planet_pop value = -0.10 }
			change_variable = { which = society_research_mult_planet_pop value = -0.10 }
			change_variable = { which = engineering_research_mult_planet_pop value = -0.10 }
			change_variable = { which = unity_mult_planet_pop value = -0.10 }
			set_timed_pop_flag = { flag = has_pop_modifier days = 3600 }
		}
	}
	if = {
		limit = { has_modifier = pop_ghost_signal_2 }
		prev = {
			change_variable = { which = minerals_mult_planet_pop value = -0.25 }
			change_variable = { which = energy_mult_planet_pop value = -0.25 }
			change_variable = { which = physics_research_mult_planet_pop value = -0.25 }
			change_variable = { which = society_research_mult_planet_pop value = -0.25 }
			change_variable = { which = engineering_research_mult_planet_pop value = -0.25 }
			change_variable = { which = unity_mult_planet_pop value = -0.25 }
			set_timed_pop_flag = { flag = has_pop_modifier days = 3600 }
		}
	}
	if = {
		limit = { has_modifier = pop_ghost_signal_3 }
		prev = {
			change_variable = { which = minerals_mult_planet_pop value = -0.40 }
			change_variable = { which = energy_mult_planet_pop value = -0.40 }
			change_variable = { which = physics_research_mult_planet_pop value = -0.40 }
			change_variable = { which = society_research_mult_planet_pop value = -0.40 }
			change_variable = { which = engineering_research_mult_planet_pop value = -0.40 }
			change_variable = { which = unity_mult_planet_pop value = -0.40 }
			set_timed_pop_flag = { flag = has_pop_modifier days = 3600 }
		}
	}
	if = {
		limit = { prev.owner = { is_machine_empire = no } }
		if = {
			limit = { has_modifier = pop_ghost_signal_4 }
			prev = {
				change_variable = { which = minerals_mult_planet_pop value = -0.55 }
				change_variable = { which = energy_mult_planet_pop value = -0.55 }
				change_variable = { which = physics_research_mult_planet_pop value = -0.55 }
				change_variable = { which = society_research_mult_planet_pop value = -0.55 }
				change_variable = { which = engineering_research_mult_planet_pop value = -0.55 }
				change_variable = { which = unity_mult_planet_pop value = -0.55 }
			}
		}
		if = {
			limit = { has_modifier = pop_ghost_signal_5 }
			prev = {
				change_variable = { which = minerals_mult_planet_pop value = -0.70 }
				change_variable = { which = energy_mult_planet_pop value = -0.70 }
				change_variable = { which = physics_research_mult_planet_pop value = -0.70 }
				change_variable = { which = society_research_mult_planet_pop value = -0.70 }
				change_variable = { which = engineering_research_mult_planet_pop value = -0.70 }
				change_variable = { which = unity_mult_planet_pop value = -0.70 }
			}
		}
		else = {
			if = {
				limit = { has_modifier = pop_ghost_signal_4_machine }
				prev = {
					change_variable = { which = minerals_mult_planet_pop value = -0.25 }
					change_variable = { which = energy_mult_planet_pop value = -0.25 }
					change_variable = { which = physics_research_mult_planet_pop value = -0.25 }
					change_variable = { which = society_research_mult_planet_pop value = -0.25 }
					change_variable = { which = engineering_research_mult_planet_pop value = -0.25 }
				}
			}
			if = {
				limit = { has_modifier = pop_ghost_signal_5_machine }
				prev = {
					change_variable = { which = minerals_mult_planet_pop value = -0.40 }
					change_variable = { which = energy_mult_planet_pop value = -0.40 }
					change_variable = { which = physics_research_mult_planet_pop value = -0.40 }
					change_variable = { which = society_research_mult_planet_pop value = -0.40 }
					change_variable = { which = engineering_research_mult_planet_pop value = -0.40 }
				}
			}
		}
	}
	if = {
		limit = { prev.owner = { is_gestalt_consciousness = no } }
		if = {
			limit = {
				 #these don't require a pop flag since an on_action_trigger exists that allows checking this.
				OR = { is_enslaved = yes is_being_purged = yes }
			}
			if = {
				limit = { has_modifier = pop_purging_labor_camps }
				prev = {
					change_variable = { which = energy_mult_planet_pop value = -0.50 }
					change_variable = { which = physics_research_mult_planet_pop value = -1 }
					change_variable = { which = society_research_mult_planet_pop value = -1 }
					change_variable = { which = engineering_research_mult_planet_pop value = -1 }
					change_variable = { which = unity_mult_planet_pop value = -1 }
				}
			}
			if = {
				limit = { has_modifier = pop_enslaved }
				prev = {
					change_variable = { which = minerals_mult_planet_pop value = 0.10 }
					change_variable = { which = food_mult_planet_pop value = 0.10 }
					change_variable = { which = energy_mult_planet_pop value = -0.33 }
					change_variable = { which = physics_research_mult_planet_pop value = -0.75 }
					change_variable = { which = society_research_mult_planet_pop value = -0.75 }
					change_variable = { which = engineering_research_mult_planet_pop value = -0.75 }
					change_variable = { which = unity_mult_planet_pop value = -0.75 }
				}
			}
			if = {
				limit = { has_modifier = pop_enslaved_domestic }
				prev = {
					change_variable = { which = energy_mult_planet_pop value = -0.33 }
					change_variable = { which = physics_research_mult_planet_pop value = -0.75 }
					change_variable = { which = society_research_mult_planet_pop value = -0.75 }
					change_variable = { which = engineering_research_mult_planet_pop value = -0.75 }
				}
			}
			if = {
				limit = { has_modifier = pop_enslaved_military }
				prev = {
					change_variable = { which = energy_mult_planet_pop value = -0.15 }
					change_variable = { which = physics_research_mult_planet_pop value = -0.5 }
					change_variable = { which = society_research_mult_planet_pop value = -0.5 }
					change_variable = { which = engineering_research_mult_planet_pop value = -0.5 }
					change_variable = { which = unity_mult_planet_pop value = -0.15 }
				}
			}
		}
		if = {
			limit = { has_modifier = 6_hour_workday }
			prev = {
				change_variable = { which = minerals_mult_planet_pop value = -0.15 }
			}
		}
		if = {
			limit = { has_modifier = hunger_strike }
			prev = {
				change_variable = { which = minerals_mult_planet_pop value = -0.4 }
			}
		}
		if = {
			limit = { has_modifier = peace_strike }
			prev = {
				change_variable = { which = minerals_mult_planet_pop value = -0.2 }
				change_variable = { which = food_mult_planet_pop value = -0.2 }
				change_variable = { which = energy_mult_planet_pop value = -0.2 }
				change_variable = { which = engineering_research_mult_planet_pop value = -0.2 }
				change_variable = { which = physics_research_mult_planet_pop value = -0.2 }
				change_variable = { which = society_research_mult_planet_pop value = -0.2 }
			}
		}
		if = {
			limit = { has_modifier = culture_shock_stone_age }
			prev = {
				change_variable = { which = minerals_mult_planet_pop value = -0.75 }
				change_variable = { which = energy_mult_planet_pop value = -0.75 }
				change_variable = { which = engineering_research_mult_planet_pop value = -1 }
				change_variable = { which = physics_research_mult_planet_pop value = -1 }
				change_variable = { which = society_research_mult_planet_pop value = -1 }
			}
		}
		if = {
			limit = { has_modifier = culture_shock_bronze_age }
			prev = {
				change_variable = { which = minerals_mult_planet_pop value = -0.50 }
				change_variable = { which = energy_mult_planet_pop value = -0.50 }
				change_variable = { which = engineering_research_mult_planet_pop value = -1 }
				change_variable = { which = physics_research_mult_planet_pop value = -1 }
				change_variable = { which = society_research_mult_planet_pop value = -1 }
			}
		}
		if = {
			limit = { has_modifier = culture_shock_iron_age }
			prev = {
				change_variable = { which = minerals_mult_planet_pop value = -0.45 }
				change_variable = { which = energy_mult_planet_pop value = -0.45 }
				change_variable = { which = engineering_research_mult_planet_pop value = -1 }
				change_variable = { which = physics_research_mult_planet_pop value = -1 }
				change_variable = { which = society_research_mult_planet_pop value = -1 }
			}
		}
		if = {
			limit = { has_modifier = culture_shock_late_medieval_age }
			prev = {
				change_variable = { which = minerals_mult_planet_pop value = -0.40 }
				change_variable = { which = energy_mult_planet_pop value = -0.40 }
				change_variable = { which = engineering_research_mult_planet_pop value = -1 }
				change_variable = { which = physics_research_mult_planet_pop value = -1 }
				change_variable = { which = society_research_mult_planet_pop value = -1 }
			}
		}
		if = {
			limit = { has_modifier = culture_shock_renaissance_age }
			prev = {
				change_variable = { which = minerals_mult_planet_pop value = -0.35 }
				change_variable = { which = energy_mult_planet_pop value = -0.35 }
				change_variable = { which = engineering_research_mult_planet_pop value = -1 }
				change_variable = { which = physics_research_mult_planet_pop value = -1 }
				change_variable = { which = society_research_mult_planet_pop value = -1 }
			}
		}
		if = {
			limit = { has_modifier = culture_shock_steam_age }
			prev = {
				change_variable = { which = minerals_mult_planet_pop value = -0.30 }
				change_variable = { which = energy_mult_planet_pop value = -0.30 }
				change_variable = { which = engineering_research_mult_planet_pop value = -1 }
				change_variable = { which = physics_research_mult_planet_pop value = -1 }
				change_variable = { which = society_research_mult_planet_pop value = -1 }
			}
		}
		if = {
			limit = { has_modifier = culture_shock_industrial_age }
			prev = {
				change_variable = { which = minerals_mult_planet_pop value = -0.25 }
				change_variable = { which = energy_mult_planet_pop value = -0.25 }
				change_variable = { which = engineering_research_mult_planet_pop value = -0.80 }
				change_variable = { which = physics_research_mult_planet_pop value = -0.80 }
				change_variable = { which = society_research_mult_planet_pop value = -0.80 }
			}
		}
		if = {
			limit = { has_modifier = culture_shock_machine_age }
			prev = {
				change_variable = { which = minerals_mult_planet_pop value = -0.20 }
				change_variable = { which = energy_mult_planet_pop value = -0.20 }
				change_variable = { which = engineering_research_mult_planet_pop value = -0.60 }
				change_variable = { which = physics_research_mult_planet_pop value = -0.60 }
				change_variable = { which = society_research_mult_planet_pop value = -0.60 }
			}
		}
		if = {
			limit = { has_modifier = culture_shock_atomic_age }
			prev = {
				change_variable = { which = minerals_mult_planet_pop value = -0.15 }
				change_variable = { which = energy_mult_planet_pop value = -0.15 }
				change_variable = { which = engineering_research_mult_planet_pop value = -0.40 }
				change_variable = { which = physics_research_mult_planet_pop value = -0.40 }
				change_variable = { which = society_research_mult_planet_pop value = -0.40 }
			}
		}
		if = {
			limit = { has_modifier = culture_shock_early_space_age }
			prev = {
				change_variable = { which = minerals_mult_planet_pop value = -0.10 }
				change_variable = { which = energy_mult_planet_pop value = -0.10 }
				change_variable = { which = engineering_research_mult_planet_pop value = -0.20 }
				change_variable = { which = physics_research_mult_planet_pop value = -0.20 }
				change_variable = { which = society_research_mult_planet_pop value = -0.20 }
			}
		}
		if = {
			limit = { has_modifier = pop_angered }
			prev = {
				change_variable = { which = society_research_mult_planet_pop value = -0.25 }
				change_variable = { which = physics_research_mult_planet_pop value = -0.25 }
				change_variable = { which = engineering_research_mult_planet_pop value = -0.25 }
				change_variable = { which = minerals_mult_planet_pop value = -0.25 }
				change_variable = { which = energy_mult_planet_pop value = -0.25 }
				change_variable = { which = food_mult_planet_pop value = -0.25 }
			}
		}
		if = {
			limit = { has_modifier = pop_pleased }
			prev = {
				change_variable = { which = society_research_mult_planet_pop value = 0.25 }
				change_variable = { which = physics_research_mult_planet_pop value = 0.25 }
				change_variable = { which = engineering_research_mult_planet_pop value = 0.25 }
				change_variable = { which = minerals_mult_planet_pop value = 0.25 }
				change_variable = { which = energy_mult_planet_pop value = 0.25 }
				change_variable = { which = food_mult_planet_pop value = 0.25 }
			}
		}
	}
	if = {
		limit = { has_modifier = withdrawal_symptoms }
		prev = {
			change_variable = { which = food_mult_planet_pop value = -0.2 }
			change_variable = { which = minerals_mult_planet_pop value = -0.2 }
			change_variable = { which = energy_mult_planet_pop value = -0.2 }
			change_variable = { which = unity_mult_planet_pop value = -0.2 }
			change_variable = { which = physics_research_mult_planet_pop value = -0.2 }
			change_variable = { which = society_research_mult_planet_pop value = -0.2 }
			change_variable = { which = engineering_research_mult_planet_pop value = -0.2 }
		}
	}
}
check_pop_species_rights = {
	if = {
		limit = { has_living_standard = { type = living_standard_chemical_bliss } }
		prev = {
			change_variable = { which = energy_mult_planet_pop value = -0.4 }
			change_variable = { which = minerals_mult_planet_pop value = -0.4 }
			change_variable = { which = food_mult_planet_pop value = -0.4 }
			change_variable = { which = unity_mult_planet_pop value = -0.4 }
			change_variable = { which = society_research_mult_planet_pop value = -0.4 }
			change_variable = { which = physics_research_mult_planet_pop value = -0.4 }
			change_variable = { which = engineering_research_mult_planet_pop value = -0.4 }
		}
	}
	if = {
		limit = { has_living_standard = { type = living_standard_academic_privilege } }
		prev = {
			change_variable = { which = society_research_mult_planet_pop value = 0.1 }
			change_variable = { which = physics_research_mult_planet_pop value = 0.1 }
			change_variable = { which = engineering_research_mult_planet_pop value = 0.1 }
		}
	}
}
